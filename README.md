# Lucky Ball Go #

This project is to implement the components of slot machine.

What is new in this project? 
---
* Implement Evaluator and Spinner
* Customized exception for slot machine
* Unit tests for Evaluator and Spinner
* Utils for creating symbols and reels

How to build the project with Maven? 
---
Under directory *lucky-ball-go*, run the command:

>mvn clean install

 How to run the project 
---
* ####Executing .jar file

After building the project with Maven, there will be a jar file from path:
>lucky-ball-go/target/lucky-ball-go-1.0.jar
  
Run the .jar file with command:
>java -jar lucky-ball-go-1.0.jar

* ####Run the application directly

Go to *LuckyBallGo.java*, and run the main function.

 Where is the result? 
---
After running the project, the result which contains symbols and payout will be shown.
>Result: [C, B, A]  
 Payout: 3000
