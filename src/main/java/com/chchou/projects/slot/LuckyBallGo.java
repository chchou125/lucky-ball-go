package com.chchou.projects.slot;

import com.chchou.projects.slot.impl.EvaluatorImpl;
import com.chchou.projects.slot.impl.SpinnerImpl;
import com.chchou.projects.slot.utils.Utils;
import com.genesis.exams.slot.Evaluator;
import com.genesis.exams.slot.SlotMachine;
import com.genesis.exams.slot.SpinResult;
import com.genesis.exams.slot.Spinner;
import com.genesis.exams.slot.Symbol;

public class LuckyBallGo {

  public static void main(String[] args) {

    Utils utils = new Utils();
    Spinner spinner = new SpinnerImpl();
    Evaluator evaluator = new EvaluatorImpl();
    SlotMachine machine = new SlotMachine(utils.initializeReels(), spinner, evaluator);
    SpinResult result = machine.spin(100);

    //Print out the result and payout
    StringBuilder sb = new StringBuilder();
    sb.append("Result: [");

    for (Symbol symbol : result.getSymbols()) {
      sb.append(symbol.getName() + ", ");
    }
    sb.setLength(sb.length() - 2);
    sb.append("]");
    System.out.println(sb.toString());
    System.out.println("Payout: " + result.getPayout());
  }

}
