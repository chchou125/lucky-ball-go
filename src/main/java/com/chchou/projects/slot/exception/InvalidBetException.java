package com.chchou.projects.slot.exception;

public class InvalidBetException extends SlotMachineException {

  public InvalidBetException() {
    super();
  }

  public InvalidBetException(String message) {
    super(message);
  }

  public InvalidBetException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidBetException(Throwable cause) {
    super(cause);
  }

  protected InvalidBetException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
