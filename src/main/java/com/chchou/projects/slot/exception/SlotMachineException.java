package com.chchou.projects.slot.exception;

public class SlotMachineException extends RuntimeException {

  public SlotMachineException() {
    super();
  }

  public SlotMachineException(String message) {
    super(message);
  }

  public SlotMachineException(String message, Throwable cause) {
    super(message, cause);
  }

  public SlotMachineException(Throwable cause) {
    super(cause);
  }

  protected SlotMachineException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
