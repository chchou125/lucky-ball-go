package com.chchou.projects.slot.impl;

import com.chchou.projects.slot.exception.InvalidBetException;
import com.genesis.exams.slot.Evaluator;
import com.genesis.exams.slot.Symbol;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class EvaluatorImpl implements Evaluator {

  @Override
  public long evaluate(Symbol[] symbols, long bet) {

    if (bet == 0) {
      return 0;
    }

    if (bet < 0) {
      throw new InvalidBetException("The amount of bet should be greater than 0");
    }

    Set<String> winningSymbolSet = new HashSet<>(Arrays.asList("A", "B", "C"));
    Set<String> resultNamesSet = Arrays.stream(symbols).map(Symbol::getName)
        .collect(Collectors.toSet());

    int symbolSetSize = resultNamesSet.size();

    // Combination AAA, BBB, CCC
    if (symbolSetSize == 1 && resultNamesSet.stream().anyMatch(winningSymbolSet::contains)) {
      return bet * 20;
    }
    // Combination ABC
    else if (resultNamesSet.containsAll(winningSymbolSet)) {
      return bet * 30;
    }
    return 0;
  }
}
