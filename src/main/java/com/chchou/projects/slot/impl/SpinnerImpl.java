package com.chchou.projects.slot.impl;

import com.chchou.projects.slot.utils.RandomCollection;
import com.genesis.exams.slot.Reel;
import com.genesis.exams.slot.Spinner;
import com.genesis.exams.slot.Symbol;

public class SpinnerImpl implements Spinner {

  @Override
  public Symbol spin(Reel reel) {

    RandomCollection<Symbol> randomCollection = new RandomCollection<>();

    for (Symbol symbol : reel.getSymbols()) {
      randomCollection.add(symbol.getWeight(), symbol);
    }

    return randomCollection.next();
  }
}
