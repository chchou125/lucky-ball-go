package com.chchou.projects.slot.utils;

import com.genesis.exams.slot.Reel;
import com.genesis.exams.slot.Symbol;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;

public class Utils {

  private Random random = new Random();

  /**
   * Generate the random array of symbols for test
   *
   * @param count the length of symbols
   * @return array of symbols
   */
  public Symbol[] generateRandomSymbols(int count) {
    Symbol[] symbols = new Symbol[count];

    for (int i = 0; i < count; i++) {
      symbols[i] = new Symbol(RandomStringUtils.random(1), random.nextInt(10));
    }

    return symbols;
  }

  /**
   * Generate the array of symbols for test
   *
   * @param count the length of symbols
   * @param names the name of symbols
   * @return array of symbols
   */
  public Symbol[] generateSameSymbols(int count, String[] names) {
    Symbol[] symbols = new Symbol[count];

    for (int i = 0; i < count; i++) {
      symbols[i] = new Symbol(names[i], 1);
    }

    return symbols;
  }

  /**
   * Initialize reels according to default setting
   *
   * @return array of reel
   */
  public Reel[] initializeReels() {
    Map<String, Integer> symbolMap = new HashMap<String, Integer>();
    String[] names = {"A", "B", "C", "X", "Y", "Z"};
    Integer[] weights = {1, 1, 1, 2, 3, 4};

    for (int i = 0; i < names.length; i++) {
      symbolMap.put(names[i], weights[i]);
    }

    String[][] symbolOrders = {
        {"A", "B", "C", "X", "Y", "Z"},
        {"Z", "Y", "X", "A", "B", "C"},
        {"A", "B", "C", "X", "Y", "Z"}
    };

    List<Reel> reelList = new ArrayList<Reel>();

    for (String[] reelOrder : symbolOrders) {
      List<Symbol> symbolList = new ArrayList<Symbol>();
      for (String symbol : reelOrder) {
        symbolList.add(new Symbol(symbol, symbolMap.get(symbol)));
      }
      reelList.add(new Reel(symbolList.toArray(new Symbol[0])));
    }
    return reelList.toArray(new Reel[0]);
  }
}

