package com.chchou.projects.slot;

import com.chchou.projects.slot.exception.InvalidBetException;
import com.chchou.projects.slot.impl.EvaluatorImpl;
import com.chchou.projects.slot.utils.Utils;
import com.genesis.exams.slot.Evaluator;
import com.genesis.exams.slot.Symbol;
import java.util.Random;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EvaluatorImplTest {

  private Evaluator evaluator;
  private Random random;
  private Symbol[] symbols;
  private Utils utils;

  @Before
  public void setUp() throws Exception {

    evaluator = new EvaluatorImpl();
    random = new Random();
    utils = new Utils();
  }

  @Test(expected = InvalidBetException.class)
  public void evaluateBetIsLessThanZeroTest() throws Exception {

    symbols = utils.generateRandomSymbols(3);
    int bet = Math.abs(random.nextInt(1000)) * (-1);

    evaluator.evaluate(symbols, bet);
  }

  @Test
  public void evaluateBetIsEqualToZeroTest() throws Exception {

    symbols = utils.generateRandomSymbols(3);
    int bet = 0;

    long payout = evaluator.evaluate(symbols, bet);

    Assert.assertEquals(payout, 0);
  }

  @Test
  public void evaluateWinningSameSymbolsTest() throws Exception {

    int bet = Math.abs(random.nextInt(1000)) + 1;
    symbols = utils.generateSameSymbols(3, new String[]{"A", "A", "A"});

    long payout = evaluator.evaluate(symbols, bet);

    Assert.assertEquals(payout, bet * 20);
  }

  @Test
  public void evaluateWinningABCTest() throws Exception {

    int bet = Math.abs(random.nextInt(1000)) + 1;
    symbols = utils.generateSameSymbols(3, new String[]{"A", "B", "C"});

    long payout = evaluator.evaluate(symbols, bet);

    Assert.assertEquals(payout, bet * 30);
  }

  @Test
  public void evaluateLostTest() throws Exception {

    int bet = Math.abs(random.nextInt(1000)) + 1;
    symbols = utils.generateSameSymbols(3, new String[]{"X", "X", "X"});

    long payout = evaluator.evaluate(symbols, bet);

    Assert.assertEquals(payout, 0);
  }
}
