package com.chchou.projects.slot;

import com.chchou.projects.slot.impl.SpinnerImpl;
import com.chchou.projects.slot.utils.Utils;
import com.genesis.exams.slot.Reel;
import com.genesis.exams.slot.Spinner;
import com.genesis.exams.slot.Symbol;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SpinnerImplTest {

  private Utils utils;
  private Spinner spinner;

  @Before
  public void setUp() throws Exception {

    utils = new Utils();
    spinner = new SpinnerImpl();
  }

  @Test
  public void spinTest() throws Exception {

    Reel reel = new Reel(utils.generateRandomSymbols(6));

    Symbol symbol = spinner.spin(reel);

    Assert.assertNotNull(symbol);
    Assert.assertNotNull(symbol.getName());
  }

}
